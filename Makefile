format:
	gofmt -s -w .

build:
	go build -o ./snippetbox.o ./cmd/web

execute:
	go run ./cmd/web/

test:
	go test ./...

deploy:
	sudo service snippetbox restart
	sudo systemctl daemon-reload

status:
	sudo service snippetbox status

default: build
