package main

import (
    "crypto/tls"
    "database/sql"
    "flag"
    "html/template"
	"log"
	"net/http"
	"os"
    "time"

    "snippetbox/internal/models"

    "github.com/alexedwards/scs/mysqlstore"
    "github.com/alexedwards/scs/v2"
    _ "github.com/go-sql-driver/mysql"
)

type application struct {
    infoLog *log.Logger
    errLog *log.Logger
    snippets *models.SnippetModel
    users *models.UserModel
    templateCache map[string]*template.Template
    sessionManager *scs.SessionManager
}

func main() {
    addr := flag.String("addr", os.Getenv("APP_PORT"), "HTTP network address")
    dsn  := flag.String("dsn", os.Getenv("SNIPPETBOX_DSN"), "MySQL data source name")
    flag.Parse()


    infoLog := log.New(os.Stdout, "INFO\t", log.Ldate|log.Ltime|log.LUTC)
    errLog := log.New(os.Stderr, "ERROR\t", log.Ldate|log.Ltime|log.Lshortfile|log.LUTC)

    db, err := openDB(*dsn)
    if err != nil {
        errLog.Fatal(err)
    }
    defer db.Close()

    templateCache, err := newTemplateCache()
    if err != nil {
        errLog.Fatal(err)
    }

    sessionManager := scs.New()
    sessionManager.Store = mysqlstore.New(db)
    sessionManager.Lifetime = 12 * time.Hour
    sessionManager.Cookie.Secure = true

    app := &application{
        infoLog: infoLog,
        errLog: errLog,
        snippets: &models.SnippetModel{DB: db},
        users: &models.UserModel{DB: db},
        templateCache: templateCache,
        sessionManager: sessionManager,
    }

    tlsConfig := &tls.Config{
        MinVersion: tls.VersionTLS13,
        MaxVersion: tls.VersionTLS13,
        CurvePreferences: []tls.CurveID{tls.X25519, tls.CurveP256},
        CipherSuites: []uint16{
            tls.TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384,
            tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
            tls.TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305,
            tls.TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305,
            tls.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
            tls.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
        },
    }

    srv := &http.Server{
        Addr: *addr,
        MaxHeaderBytes: 65536,
        ErrorLog: errLog,
        Handler: app.routes(),
        TLSConfig: tlsConfig,
        IdleTimeout: time.Minute,
        ReadTimeout: 5 * time.Second,
        WriteTimeout: 10 * time.Second,
    }

	infoLog.Printf("Starting server on port %s", *addr)
	err = srv.ListenAndServe()
	errLog.Fatal(err)
}

func openDB(dsn string) (*sql.DB, error) {
    db, err := sql.Open("mysql", dsn)
    if err != nil {
        return nil, err
    }
    if err = db.Ping(); err != nil {
        return nil, err
    }
    return db, nil
}
