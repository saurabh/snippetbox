curl https://localhost:7210/
curl https://localhost:7210/some-page

curl https://localhost:7210/snippet/view
curl "https://localhost:7210/snippet/view?id=2"
curl "https://localhost:7210/snippet/view?id=foo"

curl -i -X POST https://localhost:7210/snippet/create
curl -i -X PATCH https://localhost:7210/snippet/create

curl -i https://localhost:7210/static/img/
curl -i https://localhost:7210/static/img

curl -iL -X POST https://localhost:7210/snippet/create


curl -i "https://localhost:7210/snippet/view?id=4"
curl -i "https://localhost:7210/snippet/view?id=99"


curl -i -X POST "https://localhost:7210/snippet/view/1"
